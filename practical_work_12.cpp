﻿#include <iostream>

static int N = 42;
bool IsTrue = N % 7 != 0;

//функция из второй части задания
void PrintOddNumbers(int n, bool b)
{
	if (b)
	{
		for (int i = 0; i <= N; i += 2)
		{
			std::cout << i << " ";
		}
	}
	else
	{
		for (int i = 1; i <= N; i += 2)
		{
			std::cout << i << " ";
		}
	}
}

int main()
{
	// Первая часть домашки
	for(int i = 0; i <= N; i += 2)
	{
		std::cout << i << " ";
	}

	std::cout << "\n";

	PrintOddNumbers(N, IsTrue);

	return 0;
}